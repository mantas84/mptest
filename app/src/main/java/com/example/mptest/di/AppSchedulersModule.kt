package com.example.mptest.di

import javax.inject.Named
import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
class AppSchedulersModule {

    @Provides
    @Singleton
    @Named(AppSchedulersModule.MAIN)
    internal fun provideMainScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Singleton
    @Named(AppSchedulersModule.IO)
    internal fun provideIoScheduler(): Scheduler {
        return Schedulers.io()
    }

    @Provides
    @Singleton
    @Named(AppSchedulersModule.COMPUTATION)
    internal fun provideComputationScheduler(): Scheduler {
        return Schedulers.computation()
    }

    @Provides
    @Singleton
    @Named(AppSchedulersModule.TRAMPOLINE)
    internal fun provideTrampolineScheduler(): Scheduler {
        return Schedulers.trampoline()
    }

    @Provides
    @Singleton
    @Named(AppSchedulersModule.SINGLE)
    internal fun provideSingleScheduler(): Scheduler {
        return Schedulers.single()
    }

    companion object {
        const val MAIN = "main"
        const val IO = "io"
        const val COMPUTATION = "computation"
        const val TRAMPOLINE = "trampoline"
        const val SINGLE = "single"
    }
}
