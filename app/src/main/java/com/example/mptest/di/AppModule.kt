package com.example.mptest.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.patloew.rxlocation.RxLocation
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideSharedPrefs(appContext: Context): SharedPreferences =
        appContext.getSharedPreferences("AppPrefs", Context.MODE_PRIVATE)


    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    fun provideRxLocation(application: Application): RxLocation = RxLocation(application)

}