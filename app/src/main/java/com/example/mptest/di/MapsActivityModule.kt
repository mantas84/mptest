package com.example.mptest.di

import com.example.mptest.ui.map.MapsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class MapsActivityModule {

    @ContributesAndroidInjector()
    abstract fun contributeMapsActivity(): MapsActivity
}