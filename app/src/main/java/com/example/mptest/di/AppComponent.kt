package com.example.mptest.di

import android.app.Application
import com.example.mptest.MpTestApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        AndroidSupportInjectionModule::class,
        MapsActivityModule::class,
        NetModule::class,
        AppSchedulersModule::class
    ]

)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(mpTestApp: MpTestApp)
}