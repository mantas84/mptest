package com.example.mptest.models

data class CarModel(val id: Int, val photoUrl: String, val title: String)