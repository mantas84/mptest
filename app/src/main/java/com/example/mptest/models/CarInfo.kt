package com.example.mptest.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CarInfo(
    val batteryEstimatedDistance: Double,
    val batteryPercentage: Int,
    val id: Int,
    val isCharging: Boolean,
    val location: Location,
    @Json(name = "model")
    val carModel: CarModel,
    val plateNumber: String
)