package com.example.mptest.models

data class Location(
    val address: String,
    val id: Int,
    val latitude: Double,
    val longitude: Double
)