package com.example.mptest.ui.list

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.mptest.R
import com.example.mptest.models.CarInfo
import com.example.mptest.models.Location
import com.robinhood.ticker.TickerUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_car.view.*
import timber.log.Timber


class CarRecycleViewAdapter(var cars1: List<Pair<CarInfo, Double?>>, val clickListener: (Location) -> Unit) :
    RecyclerView.Adapter<CarRecycleViewAdapter.ViewHolder>() {

    private val cars = cars1.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_car, parent, false))
    }

    override fun getItemCount(): Int = cars.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val car = cars[position].first
        val distanceToUser = getDistanceString(cars[position].second, holder.cardImgCar.context.resources)
        holder.apply {
            Picasso.get().load(car.carModel.photoUrl).into(cardImgCarBig)
            Picasso.get().load(car.carModel.photoUrl).into(cardImgCar)
            cardTicBattery.text =
                    holder.cardImgCar.resources.getString(R.string.battery_percentage_with_int, car.batteryPercentage)
            cardTicBatteryBig.text =
                    holder.cardImgCar.resources.getString(R.string.battery_percentage_with_int, car.batteryPercentage)
            cardTxtCarTitle.text = car.carModel.title
            cardTxtCarTitleBig.text = car.carModel.title
            cardTxtPlateNumber.text = car.plateNumber
            cardTxtPlateNumberBig.text = car.plateNumber
            cardTxtEstTravelDistance.text = holder.cardImgCar.resources.getString(
                R.string.battery_distance_kilometers, car.batteryEstimatedDistance.toInt()
            )
            cardTicDistance.text = distanceToUser
            cardTicDistanceBig.text = distanceToUser
            locationIcon.setOnClickListener {
                Timber.d("locationIcon clicked")
                clickListener(car.location)
            }
            foldingCell.setOnClickListener { foldingCell.toggle(false) }
        }


    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardImgCarBig = view.img_car_big
        val cardTxtCarTitleBig = view.txt_car_title_big
        val cardTxtPlateNumberBig = view.txt_plate_number_big
        val cardTicBatteryBig = view.tic_battery_big.apply { this.setCharacterLists(TickerUtils.provideNumberList()) }
        val cardTxtEstTravelDistance = view.txt_estimated_distance_to_travel
        val cardTicDistanceBig =
            view.tic_distance_big.apply { this.setCharacterLists(TickerUtils.provideNumberList()) }
        val cardImgCar = view.img_car
        val cardTxtCarTitle = view.txt_car_title
        val cardTxtPlateNumber = view.txt_plate_number
        val cardTicBattery = view.tic_battery.apply { this.setCharacterLists(TickerUtils.provideNumberList()) }
        val cardTicDistance = view.tic_distance.apply { this.setCharacterLists(TickerUtils.provideNumberList()) }
        val foldingCell = view.folding_cell
        val locationIcon = view.img_loc_pointer
    }

    private fun getDistanceString(distance: Double?, resources: Resources): String {
        if (distance == null) return "--"
        val distanceInt = distance.toInt()
        return when (distanceInt) {
            in 0..1000 -> resources.getString(R.string.distance_meters, distanceInt)
            else -> resources.getString(R.string.distance_kilometers, (distance / 1000.0).toFloat())
        }

    }

    fun updateCars(cars: List<Pair<CarInfo, Double?>>) {
        val myDiffCallback = MyDiffCallback(cars, this.cars)
        val diffResult = DiffUtil.calculateDiff(myDiffCallback)
        this.cars.clear()
        this.cars.addAll(cars)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: List<Any>) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            val bundle = payloads[0] as Bundle
            for (key in bundle.keySet()) {
                if (key == "distance") {
                    val newDistance = bundle.getDouble("distance")
                    val distanceToUser = getDistanceString(newDistance, holder.cardImgCar.context.resources)
                    holder.cardTicDistance.text = distanceToUser
                    holder.cardTicDistanceBig.text = distanceToUser
                }
                if (key == "batteryPercentage") {
                    val newBatteryPercentage = bundle.getInt("batteryPercentage")
                    holder.cardTicBattery.text =
                            holder.cardImgCar.resources.getString(
                                R.string.battery_percentage_with_int,
                                newBatteryPercentage
                            )
                    holder.cardTicBatteryBig.text =
                            holder.cardImgCar.resources.getString(
                                R.string.battery_percentage_with_int,
                                newBatteryPercentage
                            )
                }
            }

        }
    }
}