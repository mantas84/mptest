package com.example.mptest.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mptest.R
import com.example.mptest.models.CarInfo
import com.example.mptest.ui.list.CarRecycleViewAdapter
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.clustering.ClusterManager
import com.patloew.rxlocation.RxLocation
import com.ramotion.fluidslider.FluidSlider
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.bottom_sheet_cars.*
import timber.log.Timber
import javax.inject.Inject


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MapViewModel

    private var myMap: GoogleMap? = null
    private var clusterManager: ClusterManager<MarkerData>? = null

    @Inject
    lateinit var rxLocation: RxLocation
    private var locationDisposable: Disposable? = null
    private val rxPermissions = RxPermissions(this)
    private val compositeDisposable = CompositeDisposable()

    private var snackbar: Snackbar? = null
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    private var rclAdapter: CarRecycleViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MapViewModel::class.java)
        viewModel.viewCreated()

        viewModel.viewState.observe(this, Observer { state -> renderState(state) })

        initBottomSheet()

        fab_filter.setOnClickListener {
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_HALF_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
                fabVisible(false)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
        }

        initBottomSheetButtons()

    }

    private fun initBottomSheetButtons() {
        fun createBatteryInputDialog() {
            val currentValue = if (chip_battery.isVisible) chip_battery.text.dropLast(1).toString().toInt() else 0
            val max = 100
            val min = 0
            val total = max - min
            val builder = AlertDialog.Builder(this)
            val li = LayoutInflater.from(this)
            val myView = li.inflate(R.layout.dialog_battery, null)
            val s = myView.findViewById<FluidSlider>(R.id.fluidSlider)
            Timber.d("View is null ${s == null}")
            val slider = myView.findViewById<FluidSlider>(R.id.fluidSlider)!!
            slider.positionListener = { pos -> slider.bubbleText = "${min + (total * pos).toInt()}" }
            slider.position = (currentValue / 100.0f)
            slider.startText = "$min"
            slider.endText = "$max"
            builder.setView(myView)
            builder.setTitle(R.string.battery_level_from)
            builder.setPositiveButton(
                android.R.string.ok
            ) { dialog1, id ->
                Timber.d("Slider position ${slider.position}")
                viewModel.filterMinimumBattery((slider.position * 100).toInt())
                dialog1.dismiss()

            }
            val dialog = builder.create()
            dialog.show()
        }

        fun createPlateFilterDialog() {
            val currentValue = if (chip_plate.isVisible) chip_plate.text.toString() else null
            val builder = AlertDialog.Builder(this)
            val li = LayoutInflater.from(this)
            val myView = li.inflate(R.layout.dialog_plate, null)
            val editText = myView.findViewById<EditText>(R.id.edt_plate_number)
            if (currentValue != null)
                editText.setText(currentValue)
            builder.setView(myView)
            builder.setTitle(R.string.enter_part_of_plate_number)
            builder.setPositiveButton(
                android.R.string.ok
            ) { dialog1, id ->
                val plateFilterValue: String = editText.text.toString()
                Timber.d("plate filter $plateFilterValue")
                viewModel.filterPlateNumber(if (plateFilterValue == "") null else plateFilterValue)
                dialog1.dismiss()

            }
            val dialog = builder.create()
            dialog.show()


        }

        btn_battery.setOnClickListener { createBatteryInputDialog() }
        btn_plate_number.setOnClickListener { createPlateFilterDialog() }
        btn_sort_asc_active.setOnClickListener { viewModel.sortBy(true) }
        btn_sort_desc_active.setOnClickListener { viewModel.sortBy(false) }
        btn_sort_asc_passive.setOnClickListener { viewModel.sortBy(true) }
        btn_sort_desc_passive.setOnClickListener { viewModel.sortBy(false) }
        chip_plate.setOnCloseIconClickListener { viewModel.filterPlateNumber(null) }
        chip_battery.setOnCloseIconClickListener { viewModel.filterMinimumBattery(0) }

    }

    private fun updateFilterViews(filterState: FilterState) {
        fun showAsc() {
            Timber.d("isAsc")
            btn_sort_asc_active.visibility = View.VISIBLE
            btn_sort_desc_active.visibility = View.GONE
            btn_sort_asc_passive.visibility = View.GONE
            btn_sort_desc_passive.visibility = View.VISIBLE
        }

        fun showDesc() {
            Timber.d("isDesc")
            btn_sort_asc_active.visibility = View.GONE
            btn_sort_desc_active.visibility = View.VISIBLE
            btn_sort_asc_passive.visibility = View.VISIBLE
            btn_sort_desc_passive.visibility = View.GONE
        }

        Timber.d("filter state asc ${filterState.sortByAsc}")
        if (filterState.sortByAsc && !btn_sort_asc_active.isVisible) {
            showAsc()
        } else {
            if (!filterState.sortByAsc && btn_sort_asc_active.isVisible)
                showDesc()
        }


        if (filterState.plateNumber == null) {
            if (chip_plate.visibility == View.VISIBLE) {
                chip_plate.visibility = View.GONE
            }
            chip_plate.text = ""
        } else {
            if (chip_plate.visibility == View.GONE) {
                chip_plate.visibility = View.VISIBLE
            }
            chip_plate.text = filterState.plateNumber
        }

        if (filterState.minBattery == 0) {
            if (chip_battery.visibility == View.VISIBLE) {
                chip_battery.visibility = View.GONE
            }
            chip_battery.text = "0+"
        } else {
            if (chip_battery.visibility == View.GONE) {
                chip_battery.visibility = View.VISIBLE
            }
//            chip_battery.text = "${filterState.minBattery}+"
            chip_battery.text = resources.getString(R.string.chip_battery_text, filterState.minBattery)
        }
    }

    private fun initBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN


        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {

            }

            override fun onStateChanged(bottomSheetView: View, state: Int) {
                when (state) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        fabVisible(true)
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

        })
    }

    private fun renderState(state: MapActivityState?) {
        when (state) {
            is MapActivityState.InitInternetCheck -> {
                showLoading(true)
                noInternet(false, false)
                Timber.d("MapActivityState: InitInternetCheck")
                fabVisible(false)
            }
            is MapActivityState.NoInternet -> {
                showLoading(false)
                noInternet(myMap == null, true)
                Timber.d("MapActivityState: NoInternet")
                fabVisible(false)
                updateFilterViews(state.filterState)
            }
            is MapActivityState.LoadingData -> {
                showLoading(true)
                noInternet(false, false)
                Timber.d("MapActivityState: LoadingData")
                if (!state.mapLoaded)
                    loadMap()
                fabVisible(false)
                updateFilterViews(state.filterState)
            }
            is MapActivityState.CheckGps -> {
                noInternet(false, false)
                showLoading(false)
                Timber.d("MapActivityState: CheckGps")
                askPermission()
                showGpsSnackBar(false)
                fabVisible(false)
                updateFilterViews(state.filterState)
            }
            is MapActivityState.LoadedData -> {
                noInternet(false, false)
                showLoading(false)
                Timber.d("MapActivityState: LoadedData")
                if (myMap == null) {
                    //reload on rotation
                    loadMap()
                } else {
                    if (state.gpsAllowed) {
                        showLocation(myMap!!)
                        showGpsSnackBar(false)
                    } else {
                        showGpsSnackBar(true)
                    }
                    showCarsOnMap(state.carList, myMap!!)
                    fabVisible(true)
                    updateFilterViews(state.filterState)
                }


            }
            null -> {
                //ignored, Should not happen
                Timber.d("MapActivityState: null")
            }

        }
    }

    private fun noInternet(full: Boolean, show: Boolean) {
        animation_view_no_internet.visibility = View.GONE
        animation_view_no_internet.pauseAnimation()
        if (full && show) {
            animation_view_no_internet.visibility = View.VISIBLE
            animation_view_no_internet.playAnimation()
        }
        if (show) {
            snackbar = Snackbar.make(
                root_view,
                "No Internet",
                Snackbar.LENGTH_INDEFINITE
            ).setAction("Retry?") {
                viewModel.checkConnectivity()
                snackbar?.dismiss()
            }
            snackbar?.show()
        } else {
            snackbar?.dismiss()
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            animation_view.playAnimation()
            animation_view.visibility = View.VISIBLE
        } else {
            animation_view.cancelAnimation()
            animation_view.visibility = View.GONE
        }
    }

    private fun loadMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun askPermission() {
        val permissionDisposable =
            rxPermissions.requestEach(Manifest.permission.ACCESS_FINE_LOCATION).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe { permission ->
                    when {
                        permission.granted -> {
                            viewModel.locationPermission(LocationPermissionStatus.GRANTED)
                        }
                        permission.shouldShowRequestPermissionRationale -> {
                            viewModel.locationPermission(LocationPermissionStatus.RATIONALE)
                        }
                        else -> {
                            viewModel.locationPermission(LocationPermissionStatus.NOT_GRANTED)
                        }
                    }
                }
        compositeDisposable.add(permissionDisposable)
    }

    private fun showGpsSnackBar(show: Boolean) {
        if (show) {
            snackbar = Snackbar.make(
                root_view,
                R.string.missing_location_permission,
                Snackbar.LENGTH_INDEFINITE
            ).setAction(R.string.allow_questionmark) {
                askPermission()
                snackbar?.dismiss()
            }
            snackbar?.show()
        } else {
            snackbar?.dismiss()
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        Timber.d("onMapReady")
        myMap = googleMap
        viewModel.onMapReady()
    }

    @SuppressLint("MissingPermission")
    private fun showLocation(myMap: GoogleMap) {
        if (locationDisposable == null) {
            getLocation()
        }
        myMap.isMyLocationEnabled = true

        with(myMap.uiSettings) {
            isCompassEnabled = true
            isMyLocationButtonEnabled = true
            isMapToolbarEnabled = false
        }
    }


    private fun showCarsOnMap(cars: List<Pair<CarInfo, Double?>>, myMap: GoogleMap) {
        val firstTime = clusterManager == null
        if (firstTime) {
            clusterManager = ClusterManager(this, myMap)
            val renderer = CustomClusterRenderer(this, myMap, clusterManager!!)
            clusterManager?.renderer = renderer
            myMap.setOnCameraIdleListener(clusterManager)
            rclAdapter = CarRecycleViewAdapter(cars1 = cars) { focusLocation(it) }
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = rclAdapter
        }
        rclAdapter?.updateCars(cars)
        clusterManager?.clearItems()
        clusterManager?.cluster()
        clusterManager?.addItems(cars.map { carInfo -> MarkerData(carInfo.first) })
        clusterManager?.cluster()


    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        val locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(50000)
        locationDisposable =
                rxLocation.location().updates(locationRequest).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe {
                        Timber.d("Got location $it")
                        viewModel.latestLocation(it)
                    }
        compositeDisposable.add(locationDisposable!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun fabVisible(show: Boolean) {
        if (show && bottomSheetBehavior.state == BottomSheetBehavior.STATE_HIDDEN) {
            fab_filter.show()
        } else {
            fab_filter.hide()
        }
    }

    private fun focusLocation(location: com.example.mptest.models.Location) {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        val latLng = LatLng(location.latitude, location.longitude)
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15.0f)
        myMap?.animateCamera(cameraUpdate)
    }

}