package com.example.mptest.ui.map

enum class LocationPermissionStatus {
    GRANTED, NOT_GRANTED, RATIONALE
}