package com.example.mptest.ui.map

data class FilterState(val plateNumber: String?, val minBattery: Int = 0, val sortByAsc: Boolean = true)