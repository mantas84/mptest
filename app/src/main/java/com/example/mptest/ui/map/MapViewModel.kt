package com.example.mptest.ui.map

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mptest.di.AppSchedulersModule
import com.example.mptest.models.CarInfo
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.Scheduler
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named


class MapViewModel @Inject
constructor(
    private var interactor: MapInteractor,
    @Named(AppSchedulersModule.IO) private val ioScheduler: Scheduler,
    @Named(AppSchedulersModule.MAIN) private val mainScheduler: Scheduler
) : ViewModel() {

    fun viewCreated() {
        val firstRun = compositeDisposable.size() == 0
        if (firstRun) {
            checkConnectivity()
            subscribeListeners()
        }
    }

    private val locationPermissionStatusLiveData = MutableLiveData<LocationPermissionStatus>()
    private val locationSubject: BehaviorSubject<Location> = BehaviorSubject.createDefault(Location("Dummy"))
    private val filterStateSubject: BehaviorSubject<FilterState> = BehaviorSubject.createDefault(FilterState(null))
    val viewState = MutableLiveData<MapActivityState>()

    val mapCreated: BehaviorSubject<Boolean> = BehaviorSubject.create()
    val cars: BehaviorSubject<List<CarInfo>> = BehaviorSubject.create()
    private var allCars: List<CarInfo>? = null

    private val dataSubject = BehaviorSubject.zip(
        cars,
        mapCreated,
        BiFunction<List<CarInfo>, Boolean, Pair<List<CarInfo>, Boolean>> { t1, t2 -> Pair(t1, t2) })

    private val compositeDisposable = CompositeDisposable()

    fun getCars() {

        interactor.getData().subscribeOn(ioScheduler).observeOn(mainScheduler).subscribe(object :
            SingleObserver<List<CarInfo>> {
            override fun onSuccess(carList: List<CarInfo>) {
                cars.onNext(carList)
                allCars = carList
            }

            override fun onSubscribe(d: Disposable) {}


            override fun onError(e: Throwable) {
                Timber.d("error $e")
            }

        })
    }

    fun checkConnectivity() {
        val single = ReactiveNetwork.checkInternetConnectivity().delay(500, TimeUnit.MILLISECONDS)
            .doOnEvent { t1, t2 -> viewState.postValue(MapActivityState.InitInternetCheck(false)) }
        single.subscribeOn(ioScheduler).observeOn(mainScheduler).subscribe(object :
            SingleObserver<Boolean> {
            override fun onSuccess(connected: Boolean) {
                Timber.d("Internet Connected? $connected")
                val mapCreated = mapCreated.value == true
                if (allCars == null)
                    getCars()
                val carsWithDistance = if (cars.value == null) null else calculateDistance(
                    locationSubject.value!!,
                    cars.value!!,
                    filterStateSubject.value!!
                )
                if (connected) {
                    viewState.postValue(
                        MapActivityState.LoadingData(
                            carList = carsWithDistance,
                            mapLoaded = mapCreated,
                            error = null,
                            filterState = filterStateSubject.value!!
                        )
                    )
                } else {
                    viewState.postValue(
                        MapActivityState.NoInternet(
                            carList = carsWithDistance,
                            mapLoaded = mapCreated,
                            filterState = filterStateSubject.value!!
                        )
                    )
                }
            }

            override fun onSubscribe(d: Disposable) {

            }

            override fun onError(e: Throwable) {
                Timber.d("Internet NOT Connected. $e")

            }
        })
    }

    fun locationPermission(permissionStatus: LocationPermissionStatus) {
        Timber.d("permissionStatus $permissionStatus")
        locationPermissionStatusLiveData.postValue(permissionStatus)
        val oldState = viewState.value!!
        if (oldState is MapActivityState.CheckGps) {
            viewState.postValue(
                MapActivityState.LoadedData(
                    oldState.carList,
                    oldState.mapLoaded,
                    oldState.error,
                    permissionStatus == LocationPermissionStatus.GRANTED,
                    oldState.filterState
                )
            )
        }
        if (oldState is MapActivityState.LoadedData) {
            viewState.postValue(oldState.copy(gpsAllowed = permissionStatus == LocationPermissionStatus.GRANTED))
        }
    }

    fun latestLocation(latestLocation: Location) {
        locationSubject.onNext(latestLocation)
    }

    fun onMapReady() {
        val rotationEvent = mapCreated.value == true
        if (rotationEvent) {
            //recreate
            Timber.d("onMapReady rotation")
            viewState.postValue(viewState.value)
        } else mapCreated.onNext(true)
    }

    fun calculateDistance(
        userLocation: Location,
        cars: List<CarInfo>,
        filterState: FilterState
    ): List<Pair<CarInfo, Double?>> {
        val unsortedCars = cars.filter { carInfo -> carInfo.batteryPercentage >= filterState.minBattery }
            .filter { carInfo ->
                if (filterState.plateNumber == null) true else carInfo.plateNumber.toLowerCase().contains(
                    filterState.plateNumber.toLowerCase()
                )
            }
            .map { car ->
                val distance = if (userLocation.provider == "Dummy") {
                    null
                } else {
                    val carLocation = Location("From backend")
                    carLocation.longitude = car.location.longitude
                    carLocation.latitude = car.location.latitude
                    carLocation.distanceTo(userLocation).toDouble()
                }
                Pair(car, distance)
            }
        return if (filterState.sortByAsc)
            unsortedCars.sortedBy { it.second }
        else
            unsortedCars.sortedByDescending { it.second }
    }

    private fun subscribeListeners() {
        val dataSubjectDisposable = dataSubject.filter { it.second }.subscribe {
            val carListWithDistanceFromUser =
                calculateDistance(locationSubject.value!!, it.first, filterStateSubject.value!!)
            viewState.postValue(
                MapActivityState.CheckGps(
                    carListWithDistanceFromUser,
                    it.second,
                    null,
                    false,
                    filterStateSubject.value!!
                )
            )
        }

        val locationDisposable =
            locationSubject.subscribeOn(ioScheduler).observeOn(mainScheduler)
                .subscribe {
                    if (viewState.value != null && viewState.value is MapActivityState.LoadedData) {
                        val carsWithDistance = if (cars.value == null) null else calculateDistance(
                            locationSubject.value!!,
                            cars.value!!,
                            filterStateSubject.value!!
                        )
                        val oldState = viewState.value
                        if (oldState is MapActivityState.LoadedData && carsWithDistance != null) {
                            viewState.postValue(oldState.copy(carList = carsWithDistance))
                        }
                    }
                }

        val filterStateSubjectDisposable =
            filterStateSubject.subscribeOn(ioScheduler).observeOn(mainScheduler).subscribe {
                val oldViewState = viewState.value
                val carsWithDistance =
                    if (cars.value == null) emptyList<Pair<CarInfo, Double>>() else calculateDistance(
                        locationSubject.value!!,
                        cars.value!!,
                        filterStateSubject.value!!
                    )
                if (oldViewState != null && oldViewState is MapActivityState.LoadedData) {
                    viewState.postValue(oldViewState.copy(carList = carsWithDistance, filterState = it))

                }
                if (oldViewState != null && oldViewState is MapActivityState.NoInternet) {
                    viewState.postValue(oldViewState.copy(carList = carsWithDistance, filterState = it))
                }
                if (oldViewState != null && oldViewState is MapActivityState.LoadingData) {
                    viewState.postValue(oldViewState.copy(carList = carsWithDistance, filterState = it))
                }
                if (oldViewState != null && oldViewState is MapActivityState.CheckGps) {
                    viewState.postValue(oldViewState.copy(carList = carsWithDistance, filterState = it))
                }
            }

        compositeDisposable.add(dataSubjectDisposable)
        compositeDisposable.add(locationDisposable)
        compositeDisposable.add(filterStateSubjectDisposable)

    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun filterMinimumBattery(minBattery: Int) {
        filterStateSubject.onNext(filterStateSubject.value!!.copy(minBattery = minBattery))
    }

    fun filterPlateNumber(partOfPlateNumber: String?) {
        filterStateSubject.onNext(filterStateSubject.value!!.copy(plateNumber = partOfPlateNumber))
    }

    fun sortBy(asc: Boolean) {
        if (filterStateSubject.value!!.sortByAsc != asc)
            filterStateSubject.onNext(filterStateSubject.value!!.copy(sortByAsc = asc))
    }
}