package com.example.mptest.ui.map

import com.example.mptest.api.Espark
import com.example.mptest.models.CarInfo
import io.reactivex.Single
import javax.inject.Inject

class MapInteractor @Inject constructor(
    private val espark: Espark
) {

    fun getData(): Single<List<CarInfo>> {
        return espark.getCars()
    }

}