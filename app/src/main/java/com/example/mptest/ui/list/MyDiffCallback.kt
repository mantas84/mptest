package com.example.mptest.ui.list

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.example.mptest.models.CarInfo


class MyDiffCallback(
    private var newCars: List<Pair<CarInfo, Double?>>,
    private var oldCars: List<Pair<CarInfo, Double?>>
) :
    DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldCars.size
    }

    override fun getNewListSize(): Int {
        return newCars.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldCars[oldItemPosition].first.id == newCars[newItemPosition].first.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldCars[oldItemPosition] == newCars[newItemPosition]
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldItem = oldCars[oldItemPosition]
        val newItem = newCars[newItemPosition]
        val diff = Bundle()
        if (oldItem.second != newItem.second) {
            newItem.second?.let {
                diff.putDouble("distance", it)
            }
        }
        if (oldItem.first.batteryPercentage != newItem.first.batteryPercentage) {
            diff.putInt("batteryPercentage", newItem.first.batteryPercentage)

        }
        return if (diff.size() == 0) {
            null
        } else diff
    }
}
