package com.example.mptest.ui.map

import android.content.Context
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.example.mptest.R
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.maps.android.clustering.view.DefaultClusterRenderer


class CustomClusterRenderer(
    private val context: Context, map: GoogleMap,
    clusterManager: ClusterManager<MarkerData>
) : DefaultClusterRenderer<MarkerData>(context, map, clusterManager) {

    override fun onBeforeClusterItemRendered(item: MarkerData, markerOptions: MarkerOptions?) {
        val carMarker = BitmapDescriptorFactory.fromBitmap(AppCompatResources.getDrawable(context, R.drawable.ic_car)?.toBitmap())
        markerOptions?.icon(carMarker)?.snippet("${item.carInfo.batteryPercentage}%")
    }


}