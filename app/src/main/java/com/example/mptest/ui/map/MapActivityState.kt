package com.example.mptest.ui.map

import com.example.mptest.models.CarInfo

sealed class MapActivityState {
    data class InitInternetCheck(val inProgress: Boolean) : MapActivityState()
    data class NoInternet(
        val carList: List<Pair<CarInfo, Double?>>?,
        val mapLoaded: Boolean,
        val filterState: FilterState
    ) : MapActivityState()

    data class LoadingData(
        val carList: List<Pair<CarInfo, Double?>>?,
        val mapLoaded: Boolean,
        val error: String?,
        val filterState: FilterState
    ) :
        MapActivityState()

    data class CheckGps(
        val carList: List<Pair<CarInfo, Double?>>,
        val mapLoaded: Boolean,
        val error: String?,
        val checkingGPSPermission: Boolean,
        val filterState: FilterState
    ) : MapActivityState()

    data class LoadedData(
        val carList: List<Pair<CarInfo, Double?>>,
        val mapLoaded: Boolean,
        val error: String?,
        val gpsAllowed: Boolean,
        val filterState: FilterState
    ) : MapActivityState()
}