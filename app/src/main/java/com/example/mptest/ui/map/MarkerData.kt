package com.example.mptest.ui.map

import com.example.mptest.models.CarInfo
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

data class MarkerData(val carInfo: CarInfo) : ClusterItem {
    override fun getSnippet(): String = "${carInfo.batteryPercentage} %\n ${carInfo.plateNumber}"

    override fun getTitle(): String = carInfo.carModel.title

    override fun getPosition(): LatLng = LatLng(carInfo.location.latitude, carInfo.location.longitude)

}