package com.example.mptest.api

import com.example.mptest.models.CarInfo
import io.reactivex.Single
import retrofit2.http.GET


interface Espark {
    @GET("availablecars")
    fun getCars(): Single<List<CarInfo>>
}